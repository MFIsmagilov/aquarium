import random;
class Fish(object):
    _FishTypeList = [];
    _flagReadFile = True; #один раз разрешить читать файл
    def __init__(self, 
                 fish_type, 
                 gender,
                 name = "",
                 age = 0):
        self._fishType = fish_type;
        self._gender = gender;
        self._ageOfDeath = random.randint(fish_type.getMinimumLifeTime, fish_type.getMaximumLifeTime);
        
        self._name = name;
        if age != 0:
            self._age = age;
        else:
            '''Для мальков, определяем умрет он или нет'''
            self._age = 0;
            rnd = random.randint(0, 100);
            if rnd >= self._fishType.getFishDeathsCount:
                self._ageOfDeath = random.randint(0, fish_type.getMortalityMonths);

    def modelLife(self):
        if self._age != self._ageOfDeath:
            self._age += 1;

        if self.getFishAge >= self.getFishType.getFishAgeOfPuberty:
            ageOfPuberty = self.getFishType.getFishAgeOfPuberty;
            countMonth = self.getFishType.getFishSpawingPeriod; #каждые семь месяцев нерест

            # Если(текущий возраст - Puberty)  % (через сколько месяцев) = 0 то нерест
            if (self.getFishAge - ageOfPuberty) % countMonth == 0 and self._gender == 0:
                return self.spawning();
        return [];

    def spawning(self):
        '''Возвращает список мальков'''
        listSmallFish = [];
        fish = 0;
        countSmallFish = random.randint(self.getFishType.getCountMinSmallFish, self.getFishType.getCountMaxSmallFish);
        while fish < countSmallFish:
            whatGender = random.randint(0,1);
            listSmallFish.append(Fish(self.getFishType, whatGender, age = 0));
            fish +=1;
        return listSmallFish;

    def __str__(self):
        strGender = "Мужской"; 
        if self._gender == 0: # 1 - мужик, 0 - женщина
            strGender = "Женский";
        return 'Fish name: %s\nGender: %s\nВозраст: %s\n%s' % (self._name, strGender, self._age, self._fishType);
    
    def returnStr(self):
        strGender = "Мужской"; 
        if self._gender == 0: # 1 - мужик, 0 - женщина
            strGender = "Женский";
        return 'Fish name: %s\nGender: %s\nВозраст: %s\n%s' % (self._name, strGender, self._age, self._fishType);
     
    def __eq__(self, fish2):
        return (self._age == fish2.getFishAge and 
                self._fishType == fish2.getFishType and 
                self._gender == fish2.getFishGender and 
                self._name == fish2.getFishName);
    def __getstate__(self):
        return self.__dict__
    def __setstate__(self, state):
        if Fish.flagReadFile :
            directory = '/Data/FishType/files' ;
            files = os.listdir(directory) ;
            for nameFile in files:
                file = open(nameFile, "r");
                Fish._FishTypeList.append(jsonpickle.loads(file.read()));
                file.close();
        Fish.flagReadFile = False;
        self.__dict__ = state

    @property
    def getFishAgeOfDeath(self):
        return self._ageOfDeath;
    @property
    def getFishAge(self):
        return self._age;
    @property 
    def setFishAge(self, age):
        self._age = age;
    @property
    def getFishName(self):
        return self._name;
    @property
    def getFishGender(self):
        return self._gender;
    @property 
    def getFishType(self):
        return self._fishType;
    @property
    def setFish(self, value):
        self._name = value
    @property
    def delFish(self):
        del self._name