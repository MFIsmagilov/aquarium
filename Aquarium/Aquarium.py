import random;
from Fish import Fish;
from Stats import Stats;
class Aquarium(object):
    #_fishes = [];
    def __init__(self, name, water_volume, list_fish):
        self._name = name;
        self._waterVolume = water_volume;
        self._fishes = list_fish; #[ ]
        self._filledWater = 0;
        for f in list_fish:
            self._filledWater += f.getFishType.getFishWaterVolume;

    def addFish(self, fish):
        if fish in self._fishes and self._waterVolume < self._filledWater + fish.getFishType.getFishWaterVolume:
            return False;
        else:
            self._fishes.append(fish);
            self._filledWater += fish.getFishType.getFishWaterVolume;
            return True;

    def start(self):
        allSmallFish = []
        water = 0;
        index = 0;
        for fish in self._fishes:
            listSmallFish = fish.modelLife();
            if fish.getFishAge == fish.getFishAgeOfDeath:
                self._filledWater -= fish.getFishType.getFishWaterVolume;
                self._fishes.pop(index);
                #self.killFish(fish);
            elif len(listSmallFish) != 0 :
                water += fish.getFishType.getFishWaterVolume * len(listSmallFish);
                allSmallFish.extend(listSmallFish);
                #listSmallFish.clear();
            self.water();
            index+=1;
        self._fishes.extend(allSmallFish);
        self._filledWater += water;

    def water(self):
        '''Проверяет заполненность аквариума, и если что убивает рыбок'''
        while self._filledWater > self._waterVolume:
            index = random.randint(0, len(self._fishes) - 1);
            self.killFish(self._fishes[index]);

    def killFish(self, fish):
        '''Убиваем рыбку если она есть в списке, и заполненность аквариума уменьшаем'''
        self._filledWater -= fish.getFishType.getFishWaterVolume;
        if self._fishes.count(fish) != 0:
            self._fishes.remove(fish);

    def __str__(self):
        return '  Аквариум %s\n  Объем : %s\n  Степень заполненности : %s'% (self._name, self._waterVolume, self._filledWater);

    def returnStr(self):
        return '  Аквариум %s\n  Объем : %s\n  Степень заполненности : %s'% (self._name, self._waterVolume, self._filledWater);

    def stats(self):
        d = {};
        for f in self._fishes:
            if f.getFishType.getFishTypeName not in d:
                d[f.getFishType.getFishTypeName] = 1;
            else:
                d[f.getFishType.getFishTypeName] += 1;
        key = d.keys();
        list = [];
        result = [];
        for k in key:
            countSmallFish = 0;
            countFemales = 0;
            for f in self._fishes:
                if f.getFishType.getFishTypeName == k:
                    if f.getFishAge <= f.getFishType.getFishAgeOfPuberty:
                        countSmallFish += 1;
                    if f.getFishGender == 0:
                        countFemales += 1;
            countMales = d[k] - countFemales;
            result.append(Stats(k , d[k], countSmallFish, countFemales, countMales));
        return result;

    def Info(self):
        return '''  Аквариум %s\n  Объем : %s\n  Степень заполненности : %s
  Количество рыбок :%s'''% (self._name, self._waterVolume, self._filledWater, len(self._fishes));
    @property
    def getAquariumsName(self):
        return self._name;
    @property
    def getFilledWater(self):
        return self._filledWater;
    @property
    def getWaterVolume(self):
        return self._waterVolume;
    @property
    def getCountFish(self):
        return len(self._fishes);
    @property
    def getCountFish(self):
        return len(self._fishes);

    


