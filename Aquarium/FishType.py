class FishType(object):

    def __init__(self, 
                 fish_type_name,
                 minimum_life_time,
                 maximum_life_time,  
                 water_volume, 
                 age_of_puberty,
                 spawning_period, #срок повторения нереста
                 
                 countMinSmallFish, # минимальное кол-во мальков
                 countMaxSmallFish,
                 mortalityMonths,
                 deaths_count):
        self._fishTypeName = fish_type_name;
        ###в среднем сколько живут, [min,max]
        self._minimumLifeTime = minimum_life_time;
        self._maximumLifeTime = maximum_life_time;
        ###
        self._waterVolume = water_volume;
        self._ageOfPuberty = age_of_puberty;
        self._spawingPeriod = spawning_period;

        self._countMinSmallFish = countMinSmallFish;
        self._countMaxSmallFish = countMaxSmallFish;
        self._mortalityMonths = mortalityMonths;
        self._deathsCount = deaths_count;

    def __eq__(self, fish2):
        return (self._ageOfPuberty == fish2.getFishAgeOfPuberty and
           self._deathsCount == fish2.getFishDeathsCount and
           self._minimumLifeTime == fish2.getMinimumLifeTime and
           self._maximumLifeTime == fish2.getMaximumLifeTime and
           self._minimumLifeTime == fish2.getLifeTime and
           self._spawingPeriod == fish2.getFishSpawingPeriod and
           self._waterVolume == fish2.getFishWaterVolume);

    def __str__(self):
        return '''
    Fish type      : %s
    Life time      : %s
    Water volume   : %s
    Age of puberty : %s
    Spawing period : %s
    Dearhs count   : %s
              ''' % (self._fishTypeName, 
                     self._minimumLifeTime, 
                     self._waterVolume,
                     self._ageOfPuberty,
                     self._spawingPeriod,
                     self._deathsCount);
    def returnStr(self):
        return '''
    Fish type      : %s
    Life time      : %s
    Water volume   : %s
    Age of puberty : %s
    Spawing period : %s
    Dearhs count   : %s
              ''' % (self._fishTypeName, 
                     self._minimumLifeTime, 
                     self._waterVolume,
                     self._ageOfPuberty,
                     self._spawingPeriod,
                     self._deathsCount);


    @property
    def getMortalityMonths(self):
        return self._mortalityMonths;
    @property
    def getCountMinSmallFish(self):
        '''Минимальное кол-во мальков'''
        return self._countMinSmallFish;
    @property
    def getCountMaxSmallFish(self):
        '''Максимальное кол-во мальков'''
        return self._countMaxSmallFish;
    @property
    def getMinimumLifeTime(self):
        return self._minimumLifeTime;
    @property
    def getMaximumLifeTime(self):
        return self._maximumLifeTime;
    @property
    def getFishTypeName(self):
        return self._fishTypeName;
    @property
    def getLifeTime(self):
        return self._minimumLifeTime;
    @property
    def getFishWaterVolume(self):
        return self._waterVolume;
    @property
    def getFishAgeOfPuberty(self):
        return self._ageOfPuberty;
    @property
    def getFishSpawingPeriod(self):
        return self._spawingPeriod;
    @property
    def getFishDeathsCount(self):
        return self._deathsCount;