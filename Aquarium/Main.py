from Fish import Fish
from FishType import FishType
from Aquarium import Aquarium;
from User import User;
import json;
import jsonpickle;
from pprint import pprint
from os import listdir
jsonpickle.set_encoder_options('json', sort_keys=True, indent=4)

import jsonpickle;
from os import listdir
jsonpickle.set_encoder_options('json', sort_keys=True, indent=4)


def commands(x, person, name):
    #        "/aquarium  " : "Отобразить список аквариумов",
    #        "/add       " :  "Добавить аквариум",
    #        "/fish      " : "Доступные рыбки",
    #        "/stats Name" : "Получить статистику" ,
    #        "/addfish   " : "Добавить рыбку в аквариум" ,
    #        "/info      " : "Информация об аквариуме",
    #        "/save      " : "Сохранение",
    #        "/modellife countMonth" :"Моделирование жизни"
    if x == "/aquarium":
        aquarium(person);
    elif x == "/add":
        add(person);
    elif x.find("/stats", 0, len(x)) > -1:
            stats(person,x);
    elif x == "/fish":
        fish();
    elif x == "/addfish":
        addFish(person);
    elif x.find("/info", 0, len(x)) > -1:
        info(person,x);
    elif x == "/save":
        save(person,name);
    elif x.find("/modellife", 0, len(x)) > -1:
        modellife(person, x);

def aquarium(person):
    print(person.getInfoAquariums());

def add(person):
    name = input("Введите имя аквариума : ");
    water = input("Объем аквариума :");
    person.addAquariums(name, int(water));

def fish():
    files = listdir("Data/FishType/");
    for f in files:
        file = open("Data/FishType/" + f, "r", encoding="utf-8");
        fish = jsonpickle.decode(file.read());
        print(fish);
        file.close();
def stats(person, x):
    list = person.getStatsAquariums(x[7: len(x)]);
    for i in list:
        print(i);
    return 0;

def addFish(person):
    nameAqurium = input("Введите имя аквариума: ");
    fishType = input("Введите тип: ");
    file = open("Data/FishType/"+fishType + ".json", "r", encoding="utf-8");
    fishTypeE = jsonpickle.decode(file.read());
    file.close();
    fishName = input("Введите имя: ");
    fishAge = input("Введите возраст: ");
    fishGender = input("Введите пол: ") 
    gender = 0;
    if fishGender == "Мужской" or fishGender == "мужской" or fishGender == "М" or fishGender == "м":
        gender = 1;
    if fishGender == "Женский" or fishGender == "женский" or fishGender == "Ж" or fishGender == "ж":
        gender = 0;
    person.addFish(nameAqurium, Fish(fishTypeE, gender, age = int(fishAge,10)));
    return 0;

def info(person,x):
    person.getInfoAquariumByName(x[6:len(x)]);
    
def save(person,name):
    file = open("Data/Users/"+name + ".json", "w", encoding="utf-8");
    file.write(jsonpickle.encode(person));
    file.close();

def modellife(person, x):
    p = 1;
    countMonths = int(x[11: len(x)]);
    while p < countMonths + 1 :
        print("Моделирование месяца #", p)
        person.start();
        p+=1;
    return 0;

'''Начало'''
newUser = input("Новый пользователя? Yes/No (Y/N)")
name = "";
person = "";
if newUser == "Yes" or newUser == "y":
    name = input("Введите имя: ");
    person = User([], name);
    file = open("Data/Users/" + name + ".json", "w", encoding = "utf-8")
    file.write(jsonpickle.encode(person));
    file.close();
else:
    name = input("Вы кто? ");
    f = open("Data/Users/"+ name + ".json", "r", encoding="utf-8");
    person = jsonpickle.decode(f.read());
    f.close();


print("Приветсвую! " + name);
print("Вот список команд. Пример команды : /addfish"),

f = open("Data/Commands/console.json", "r", encoding="utf-8");
d = jsonpickle.decode(f.read());
f.close();
pprint(d);
c = "";
while c != "stop":
    c = input();
    commands(c, person, name);