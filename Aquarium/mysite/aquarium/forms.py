# -*- coding: utf-8 -*-

from django import forms
from aquarium.models import *
from django.contrib import auth


class TransplantFishForm(forms.ModelForm):
	#aquarium = forms.ModelChoiceField(queryset = Aquarium.objects.filter(user))
	class Meta:
		model = Fish
		fields = ['aquarium']


class AddFishForm(forms.ModelForm):
	class Meta:
		model = Fish
		fields = ['fishType', 'gender', 'name', 'age']
		#widgets = {'aquarium' : forms.HiddenInput(attrs = {'value': 1})}
		#widgets = {'aquarium' : forms.HiddenInput()}

class AddFishTypeForm(forms.ModelForm):
	class Meta:
		model = FishType
		fields = '__all__'

class ChangeAquariumForm(forms.ModelForm):
	class Meta:
		model = Aquarium
		fields = ['name', 'waterVolume']
		#widgets = {'id' : forms.HiddenInput(attrs = {'value': 1}), 'user' : forms.HiddenInput(attrs = {'value': 1})}

class AddAquariumForm(forms.ModelForm):
    class Meta:
        model = Aquarium
        widgets = {'user' : forms.HiddenInput(attrs = {'value': 1})}
        fields = ['name', 'waterVolume', 'user']

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100);