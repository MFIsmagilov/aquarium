from django.contrib import admin

from aquarium.models import Aquarium
from aquarium.models import Fish
from aquarium.models import FishType


admin.site.register(Aquarium)
admin.site.register(Fish)
admin.site.register(FishType)