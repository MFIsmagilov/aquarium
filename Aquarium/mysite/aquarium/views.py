# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.http import HttpResponse
from django.views.generic import View

from django.views.generic.base import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.forms.formsets import formset_factory
from django.contrib import auth
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

from aquarium.models import *
from aquarium.forms import *

@login_required
def ChangeAquariums(request, aquarium_id):
    if request.POST:
        form = ChangeAquariumForm(request.POST)
        if form.is_valid():
            aquarium = form.save(commit = False)
            aquarium.id = aquarium_id
            aquariumTemp = Aquarium.objects.get(id = aquarium_id)
            aquarium.user = aquariumTemp.user
            aquarium.filledWater = aquariumTemp.filledWater
            aquarium.save();
    return redirect("/aquarium/" + aquarium_id)

@login_required
def GenerateLifeNotId(request):
    print(request.POST)
    if request.POST:
        user = auth.get_user(request);
        a = auth.get_user(request).aquarium_set.get(name =request.POST['aquaName']);
        #a = Aquarium.objects.get(name = request.POST['aquaName'])
        a.start()
    return redirect("/aquariums/")

@login_required
def GenerateLife(request, aquarium_id, page_number = 1):
    if request.POST:
        user = auth.get_user(request);
        a = auth.get_user(request).aquarium_set.get(id = aquarium_id);
        #a = Aquarium.objects.get(id = aquarium_id)
        count = request.POST['CountLife']
        if count.isdigit():
            count = int(count);
        else:
            count = 1;
        a.start(int(count))
        page_number = str(page_number);
    return redirect("/aquarium/" + aquarium_id + "/page/" + page_number + "/");

@login_required
def DeleteAquariums(request):
    if request.POST:
        a = Aquarium.objects.get(name = request.POST['aquaName'])
        a.delete()
    return redirect("/aquariums/")


@login_required
def AddAquariums(request):
    if request.POST:
        form = AddAquariumForm(request.POST)
        print(request.POST)
        if form.is_valid():
            name = form.save(commit=False)
            name.user = auth.get_user(request);
            print(auth.get_user(request))
            print(name)
            name.save()
    return redirect("/aquariums/")
@login_required
def ClearAquarium(request, aquarium_id, page_number = 1):
    if request.POST:
        aquarium = Aquarium.objects.get(id = aquarium_id);
        aquarium.clear();

    page_number = str(page_number);
    return redirect("/aquarium/" + aquarium_id + "/")

@login_required
def Aquariums(request):
    print("<!>@>$@$#>")
    print(auth.get_user(request))
    print("<!>@>$@$#>")
    addAquariumForm = AddAquariumForm
    args = {}
    args.update(csrf(request))
    args['formAddAquarium'] = addAquariumForm
    aquarium = auth.get_user(request).aquarium_set.all();
    args['aquariums'] = aquarium; #Aquarium.objects.all()[:5]
    args['username'] = auth.get_user(request).username
    return render_to_response("aquariums\\aquariums.html", args);

@login_required
def AquariumOne(request, aquarium_id, page_number = 1):
    print("<!>@>$@$#>")
    print(request)
    print("<!>@>$@$#>")

    changeAquariumForm = ChangeAquariumForm
    transplantFishFrom = TransplantFishForm(request.POST)
    #transplantFishFrom.aquarium = Aquarium.objects.filter(user = auth.get_user(request))#auth.get_user(request).aquarium_set.all()
    transplantFishFrom.fields['aquarium'] = forms.ModelChoiceField(queryset = Aquarium.objects.filter(user = request.user))
    print(transplantFishFrom)
    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    #Aquarium.objects.filter(user = ))
    addFish = AddFishForm()
    args = {}
    args.update(csrf(request))
    #args['formDeleteAquarium'] = deleteAquariumForm
    a = auth.get_user(request).aquarium_set.get(id = aquarium_id)
    args['user'] = a.getUser
    args['id'] = a.getId
    args['name'] = a.name
    args['changeAquariumForm'] = changeAquariumForm
    args['transplantFishFrom'] = transplantFishFrom

    args['addFish'] = addFish
    args['info'] = a.__str__();
    args['statistic'] = a.getStatistic();

    fishes = a.fish_set.all();
    current_page = Paginator(fishes, 5)
    args['fishes'] = current_page.page(page_number) #fishes
    args['username'] = auth.get_user(request).username

    return render_to_response("aquariums\\aquarium.html", args);

@login_required
def AddFish(request,aquarium_id,page_number = 1):
    if request.POST:
        form = AddFishForm(request.POST)
        print(form)
        print(form.is_valid())
        if form.is_valid():
            print("ПРошел valid")
            fish = form.save(commit = False)
            aquarium = auth.get_user(request).aquarium_set.get(id= aquarium_id) #Aquarium.objects.get(id = aquarium_id)
            aquarium.addFish(fish)
            #fish.aquarium = aquarium
            #fish.save()
            page_number = str(page_number)
    return redirect("/aquarium/" + aquarium_id + "/page/" + page_number + "/")

def TransplantFish(request,aquarium_id, fish_id, page_number = 1):
    if request.POST:
        form = TransplantFishForm(request.POST);
        print("FFFFFFFFFFFFFFFFFF")
        if form.is_valid():
            fish = Fish.objects.get(id = fish_id)
            aquarium1 = Aquarium.objects.get(id = aquarium_id);
            aquarium2 = Aquarium.objects.get(id = request.POST['aquarium'])
            #aquarium2.addFish(fish);
            aquarium1.deleteFish(fish)
            aquarium2.filledWater += fish.fishType.waterVolume
            aquarium2.save();
            fish.transplant(aquarium2)
            page_number = str(page_number);
    return redirect("/aquarium/" + aquarium_id + "/page/" + page_number + "/")

def DeleteFish(request, aquarium_id, fish_id, page_number = 1):
    if request.POST:
        fish = Fish.objects.get(id = fish_id);
        aquarium = Aquarium.objects.get(id = aquarium_id);
        aquarium.deleteFish(fish);
        fish.delete();

        page_number = str(page_number);
    return redirect("/aquarium/" + aquarium_id + "/page/" + page_number + "/")
def AddFishType(request):
    if request.POST:
        form = AddFishTypeForm(request.POST)
        if form.is_valid():
            name = form.save(commit=False)
            form.save()
    return redirect("/type_fishes/")
@login_required
def FishTypes(request):
    addFishType = AddFishTypeForm
    args = {}
    args.update(csrf(request))
    types = FishType.objects.all()
    args['formAddFishType'] = addFishType
    args['types'] = types;
    args['username'] = request.user.username
    return render_to_response("aquariums\\type_fishes.html", args);



def RedirectLogin(request):
    if request.user.is_authenticated():
        return redirect("/aquariums/")
    return redirect("/accounts/login/")
