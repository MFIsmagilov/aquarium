from django.db import models
import django
from django.contrib.auth.models import User
import random;

class FishType(models.Model):
    fishTypeName = models.CharField(max_length = 100, verbose_name='Имя типа')
    minimumLifeTime = models.IntegerField(verbose_name='Минимальный возраст')
    maximumLifeTime = models.IntegerField(verbose_name='Максимальный возраст')
    waterVolume = models.IntegerField(verbose_name='Требуемый объем воды')
    ageOfPuberty = models.IntegerField(verbose_name='Возраст половой зрелости')
    spawingPeriod = models.IntegerField(verbose_name='Период нереста')
    countMinSmallFish = models.IntegerField(verbose_name='Минимальное кол-во мальков')
    countMaxSmallFish = models.IntegerField(verbose_name='Максимальное кол-во мальков')
    mortalityMonths = models.IntegerField(verbose_name='Возраст смерти мальков')
    deathsCount = models.IntegerField(verbose_name='Процент смертности мальков')
    def __str__(self):
        return self.fishTypeName
    def GetInformation(self):
        return """
Название     : %s
Время жизни  : %s - %s
Кол-во воды  : %s
Зрелость     : %s
Период размножения   : %s
Процент смертности   : %s
            """ % (self.fishTypeName, 
            self.minimumLifeTime, 
            self.maximumLifeTime, 
            self.waterVolume,
            self.ageOfPuberty,
            self.spawingPeriod,
            self.deathsCount);
    def information(self):
        return str(self.minimumLifeTime) + " | " + str(self.maximumLifeTime) + " | " + str(self.waterVolume) + " | " + str(self.ageOfPuberty) + " | " + str(self.spawingPeriod) + " | " + str(self.countMinSmallFish) + " | " + str(self.countMaxSmallFish) + " | " + str(self.mortalityMonths) + " | " + str(self.deathsCount) + " | "


class Aquarium(models.Model):
    user = models.ForeignKey(django.contrib.auth.models.User, verbose_name='Пользователь')
    name = models.CharField(max_length = 100, verbose_name='Имя аквариума')
    waterVolume = models.IntegerField(verbose_name='Объем аквариума')
    filledWater = models.IntegerField(default=0)
    countIteration = models.IntegerField(default=0, verbose_name = 'Кол-во циклов')
    
    def getUser(self):
        return self.user;
    def change(self, newName, newWaterVolume):
        self.name = newName;
        self.waterVolume = newWaterVolume;
        self.save();

    def getId(self):
        return self.id;
    def getName(self):
        return self.name;
    def clear(self):
        Fish.objects.filter(aquarium_id = self).delete();
        self.filledWater = 0;
        self.countIteration = 0;
        self.save();
    def getListFishes(self): 
        return self.fish_set.all();

    def createFish(self,aquarium_id, fish_type, gender,name = "", age = 0):
        ageOfDeath = random.randint(fish_type.minimumLifeTime, fish_type.maximumLifeTime);
        ageFish = 0;
        id = self.getFishTypeId(fish_type);
        if id != 0:
            if age != 0:
                ageFish = age;
            else:
                localFishType = FishType.objects.get(id = id);
                '''Для мальков, определяем умрет он или нет'''
                self._age = 0;
                rnd = random.randint(0, 100);
                if rnd >= localFishType.deathsCount:
                    ageOfDeath = random.randint(0, localFishType.mortalityMonths);
            f = Fish(fishType_id = id, aquarium_id = aquarium_id, gender = gender, ageOfDeath = ageOfDeath, name = name, age = age);
            f.save();

    def addFish(self, fish_type, gender, name = "", age = 0):
        #self.createFish(self.id, fish_type, gender, name, age);
        self.filledWater += fish_type.waterVolume;
        self.save();

    def deleteFish(self, fish):
        self.filledWater -= fish.fishType.waterVolume;
        self.save();

    def addFish(self, fish):
        print("2______________")    
        print(fish)
        print("2______________")
        self.createFish(self.id, fish.fishType, fish.gender, fish.name, fish.age)
        self.filledWater += fish.fishType.waterVolume;
        self.save()

    def start(self, countModelLife = 1):
        self.countIteration += countModelLife;
        numberIteration = 0;
        filledWater = self.filledWater;
        while numberIteration < countModelLife:
            fishes = Fish.objects.filter(aquarium = self);
            listSmallFishes = [];
            for fish in fishes:
                if fish.timeToDie():
                        filledWater -= fish.fishType.waterVolume;
                        fish.delete();
                        continue;
                else:
                    tempList = fish.modelLife(filledWater, self.waterVolume);
                    listSmallFishes.extend(tempList);
            j = 0;
            while j < len(listSmallFishes) and self.waterVolume > filledWater:
                filledWater += listSmallFishes[j].fishType.waterVolume;
                listSmallFishes[j].save();
                j+=1;
            numberIteration+=1;
        self.filledWater = filledWater;
        self.save();
        return "";

    def start1(self, countModelLife = 1):
        i = 0;
        filledWater = self.filledWater;
        while i < countModelLife:
            print(str(i));

            listSmallFishes = [];
            fishes = self.getListFishes();

            indexForFish = 0;
            while self.filledWater > self.waterVolume:
                number = random.randint(0, 1);
                if number == 1:
                    self.filledWater -= fishes[indexForFish].fishType.waterVolume
                    fishes[indexForFish].delete();
                indexForFish+=1;
                if indexForFish == len(fishes):
                    indexForFish = 0;
            for fish in fishes:
                if fish.timeToDie():
                    filledWater -= fish.fishType.waterVolume;
                    fish.delete();
                    continue;                
                tempList = fish.modelLife(filledWater, self.waterVolume);
                listSmallFishes.extend(tempList);            
            j = 0;
            while j < len(listSmallFishes) and self.waterVolume > filledWater:
                filledWater += listSmallFishes[j].fishType.waterVolume;
                listSmallFishes[j].save();
            i+=1;
        self.filledWater = filledWater;
        self.save();
        return "";
    def water(self, filledWater):
        fishes = self.getListFishes();
        number = random.randint(0, int(len(fishes)/2));
        i = 0;
        while i < number:
            indexFish = random.randint(1, len(fishes));
            filledWater -= fishes[indexFish].fishType.waterVolume;
            fishes[indexFish].delete();
            i+=1;

    def getStatistic(self):
        d = {};
        fishes = self.getListFishes();
        countSmallFish = 0;
        countFemales = 0;
        for fish in fishes:
            if fish.fishType.fishTypeName not in d:
                d[fish.fishType.fishTypeName] = 1;
            else:
                d[fish.fishType.fishTypeName] += 1;
            if fish.age < fish.fishType.ageOfPuberty:
                countSmallFish+=1;
            if fish.gender == 0:
                countFemales+=1;
        d["Кол-во мальков "] = countSmallFish;
        d["Кол-во самок "] = countFemales;
        d["Кол-во итераций"] = self.countIteration;
        return d;

    #def getListFishes(self):
    #    return Fish.objects.filter(aquarium_id = self.id);

    def getFishTypeId(self, fish_type):
        from django.db import connection 
        cursor = connection.cursor() 
        cursor.execute("SELECT fihsType.id FROM aquarium_fishtype fihsType WHERE fihsType.fishTypeName = \"" + fish_type.fishTypeName + "\";") 
        id = 0;
        for row in cursor.fetchall():
            id = row[0];
        return id;

    def __str__(self):
        return '  Аквариум: %s\n  Объем: %s\n  Степень заполненности: %s'%(self.name, self.waterVolume, self.filledWater)

class Fish(models.Model):
    fishType = models.ForeignKey(FishType, verbose_name='Тип')
    aquarium = models.ForeignKey(Aquarium)
    gender = models.IntegerField(choices =((0,"Женский"), (1, "Мужской")), verbose_name='Пол');
    ageOfDeath = models.IntegerField()
    name = models.CharField(max_length = 100, verbose_name='Имя')
    age = models.IntegerField(verbose_name='Возраст')

    def transplant(self, aquarium_id, aquarium_name):
        self.aquarium = Aquarium.objects.get(id = aquarium_id, name = aquarium_name)
        self.save();

    def transplant(self, aquarium):
        self.aquarium = aquarium;
        self.save();
    def modelLife(self, filledWater, waterVolume):
        self.age += 1;
        self.save();
        if self.age >= self.fishType.ageOfPuberty:
            ageOfPuberty1 = self.fishType.ageOfPuberty;
            countMonth = self.fishType.spawingPeriod; #каждые семь месяцев нерест
            # Если(текущий возраст - Puberty)  % (через сколько месяцев) = 0 то нерест
            if (self.age - ageOfPuberty1) % countMonth == 0 and self.gender == 0 and filledWater < waterVolume:
                return self.spawning();
        return [];
    def spawning(self):
        '''Возвращает список мальков'''
        fish = 0;
        countSmallFish = random.randint(self.fishType.countMinSmallFish, self.fishType.countMaxSmallFish);
        count = 0;
        listSmallFish = [];
        #print("count = " + str(countSmallFish));
        while fish < countSmallFish:
            whatGender = random.randint(0,1);
            rnd = random.randint(0, 100);
            if rnd >= self.fishType.deathsCount:
                ageOfDeath1 = random.randint(1, self.fishType.maximumLifeTime);
                # countZv = self.name.count('*');
                # newFishName = "";
                # if countZv > 0:
                #     index = self.name.find("*", 0, len(self.name))
                #     newFishName = self.name[0: index] + "[x" + "(" + str(countZv + 1) + ")]";
                # else:
                #     newFishName += self.name + "*"
                index1 = self.name.find("[", 0, len(self.name))
                newFishName = "";
                if index1 == -1:
                    newFishName = self.name + " [x1]";
                else:
                    index2 = self.name.find("]", index1, len(self.name))
                    temp = self.name[index1 + 2:index2];

                    number = int(temp) + 1;
                    newFishName = self.name[0:index1] + "[x" + str(number) + "]"

                f = Fish(fishType = self.fishType, 
                         aquarium = self.aquarium, 
                         gender = whatGender, 
                         ageOfDeath = ageOfDeath1, 
                         name=newFishName, 
                         age = 0);
                #f.save();
                listSmallFish.append(f);
            fish +=1;
                #count+=1;# которые появлись все-таки на свет
        return listSmallFish;


    def timeToDie(self):
        if self.age >= self.ageOfDeath:
            return True;
        return False;

    def die(self):
        self.delete();

    def dict(self):
        strGender = "Мужской"
        if self.gender == 0: # 1 - мужик, 0 - женщина
            strGender = "Женский"
        return {"id" : self.id, "Имя рыбки" : self.name, "Пол" : strGender, "Возраст" : self.age, "Тип" : self.fishType};
    def __str__(self):
        strGender = "Мужской" 
        if self.gender == 0: # 1 - мужик, 0 - женщина
            strGender = "Женский"
        return 'Fish name: %s.\nGender: %s.\nВозраст: %s.\n%s' % (self.name, strGender, self.age, self.fishType);