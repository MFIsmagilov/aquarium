from django.db import models
from .models import *;

class AquariumManager(models.Manager):
    def countFish(self):
        from django.db import connection;
        cursor = connection.cursor();
        cursor.execute("""
            SELECT name
            FROM aquarium_fish af, aa
            WHERE af.aquarium_id = aa.id""");
        result_list = [];
        for row in cursor.fetchall():
            p.num_responses = row[0];
            result_list.append(p);
        return result_list;