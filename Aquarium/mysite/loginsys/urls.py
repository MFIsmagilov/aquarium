from django.conf.urls import url, patterns
from django.contrib import admin
from loginsys.views import *

urlpatterns = [
    url(r'^login/$', login),
    url(r'^logout/$', logout),
    url(r'^register/$', register),
]

