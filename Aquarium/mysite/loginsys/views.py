# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.core.context_processors import csrf


def login(request):
	if request.user.is_authenticated():
		return redirect('/')
	args = {}
	args.update(csrf(request))
	if request.POST:
		username = request.POST.get('username', '')
		password = request.POST.get('password', '')
		user = auth.authenticate(username = username, password = password)
		if user is not None:
			auth.login(request, user)
			return redirect('/aquariums/')
		else:
			args['login_error'] = "Пользователь не найден"
			return render_to_response("login.html", args)
	else:
		return render_to_response('login.html', args)

def logout(request):
	auth.logout(request)
	return redirect("/accounts/login/")

def register(request):
	if request.user.is_authenticated():
		return redirect('/')
	args = {}
	args.update(csrf(request))
	args['form'] = UserCreationForm()
	if request.POST:
		newUserForm = UserCreationForm(request.POST)

		if newUserForm.is_valid():
			print("!!!!!!!!!!!!!!!!!!!!!!!!!")
			newUserForm.save()
			newUser= auth.authenticate(username = newUserForm.cleaned_data['username'], password=newUserForm.cleaned_data['password2'])
			auth.login(request, newUser)
			return redirect('/')
		else:
			args['form'] = newUserForm

	return render_to_response('register.html', args)