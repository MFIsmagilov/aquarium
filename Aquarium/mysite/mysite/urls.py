"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, patterns, include
from django.contrib import admin
from aquarium.views import *

urlpatterns = [
    url(r'^$',RedirectLogin),
    url(r'^admin/', admin.site.urls),
    url(r'^aquariums/$', Aquariums, name='Aquariums'),
    url(r'^type_fishes/$', FishTypes, name='FishType'),
    url(r'^type_fishes/add_type_fish/$', AddFishType, name='AddFishType'),


    url(r'^aquariums/add_aquarium/$', AddAquariums, name='AddAquariums'),
    url(r'^aquariums/delete_aquarium/$', DeleteAquariums, name='DeleteAquariums'),
    url(r'^aquariums/generate_aquarium/$', GenerateLifeNotId),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/generate_aquarium/$', GenerateLife),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/$', AquariumOne),

    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/generate_aquarium/$', GenerateLife),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/$', AquariumOne),    
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/change_aquarium/$', ChangeAquariums),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/transplant_fish/(?P<fish_id>[0-9]+)/$', TransplantFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/delete_fish/(?P<fish_id>[0-9]+)/$', DeleteFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/add_fish/$', AddFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/page/(?P<page_number>[0-9]+)/clear_aquarium/$', ClearAquarium),

    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/change_aquarium/$', ChangeAquariums),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/add_fish/$', AddFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/transplant_fish/(?P<fish_id>[0-9]+)/$', TransplantFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/delete_fish/(?P<fish_id>[0-9]+)/$', DeleteFish),
    url(r'^aquarium/(?P<aquarium_id>[0-9]+)/clear_aquarium/$', ClearAquarium),


    #url(r'^aquariums/(?P<aquarium_name>[a-z]+)$', AquariumOne, name='AquariumOne'),
    #url(r'^aquariums/(?P<aquarium_id>[0-9]+)$', AquariumOne, name='AquariumOne1'),


    url(r'^accounts/', include('loginsys.urls')),
]

