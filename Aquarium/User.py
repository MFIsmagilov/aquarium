from Aquarium import Aquarium;

import json;

class User(object):

    _idUser = 1; 
    _nameUser ="";
    _aquariums = [];

    def __init__(self, Aquariums, nameUser):
        self._idUser += 1;
        self._aquariums = Aquariums;
        self._nameUser = nameUser;

    def addAquariums(self, name, waterVolume):
        self._aquariums.append(Aquarium(name, waterVolume, []));

    def addFish(self, nameAquariums, fish):
        for i in self._aquariums:
            if i.getAquariumsName == nameAquariums:
                i.addFish(fish);
                return True;
        return False;

    def start(self):
        for a in self._aquariums:
            a.start();

    def getInfoAquariums(self):
        s = "";
        for a in self._aquariums:
            s+=str(a)
            s+="\n";
        return s;

    def __str__(self):
        s = self._nameUser + "\n";
        s += self.getInfoAquariums();
        return s;

    def getInfoAquariumByName(self, nameAquariums):
        '''Получить информацию аквариума по имени'''
        for a in self._aquariums:
            if a.getAquariumsName == nameAquariums:
                return str(a);
        return "Аквариум не найден";

    def getStatsAquariums(self, nameAquariums):
        for a in self._aquariums:
            if a.getAquariumsName == nameAquariums:
                return a.stats();

    @property
    def getUserName(self):
        return self._nameUser;
    @property
    def getCountAquariums(self):
        return len(_aquariums);

    @property
    def getIdUser(self):
        return self._idUser;

